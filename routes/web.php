<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'v1'], function () use ($app) {
    $app->get('version', function () use ($app) {
        return "Version 1";
    });

    resource('user', 'API\v1\UserController');
    resource('company', 'API\v1\CompanyController');
    resource('mask', 'API\v1\MaskController');
    resource('bundle', 'API\v1\BundleController');
    resource('balance', 'API\v1\BalanceController');
    resource('balance-history', 'API\v1\BalanceHistoryController');
    resource('campaign', 'API\v1\CampaignController');
    resource('base', 'API\v1\BaseListController');

    $app->post('authenticate', 'API\v1\AuthController@authenticate');

    $app->get('send-quick', 'API\v1\CampaignController@sendQuickMessage');
    $app->get('get-quick', 'API\v1\CampaignController@getQuickMessages');
    $app->get('get-logs', 'API\v1\CampaignController@getLogs');
    $app->get('get-quick-logs', 'API\v1\CampaignController@getQuickLogs');
    $app->get('download-base', 'API\v1\BaseListController@getBaseList');
});

function resource($uri, $controller){
    global $app;

    $app->get($uri, $controller.'@index');
    $app->post($uri, $controller.'@store');
    $app->get($uri.'/{id}', $controller.'@show');
    $app->put($uri.'/{id}', $controller.'@update');
    $app->patch($uri.'/{id}', $controller.'@update');
    $app->delete($uri.'/{id}', $controller.'@destroy');
}