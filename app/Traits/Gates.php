<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait Gates {
    public function verifyPermissions(Request $request, $id, $model, $gates) {
        $model = $model->find($id);

        if (!in_array('IsAdmin', $gates) && !$model)
            abort(404, "Could not find the requested resource.");

        foreach ($gates as $gate) {
            if (!$request->user()->can($gate, $model)) {
                abort(401, "Unauthorized.");
            }
        }
    }
}
