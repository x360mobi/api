<?php

namespace App\Helpers;

class ZongHelper {
    private $client;

    public function __construct() {
        $url = env('URL_ZONG');

        ini_set("soap.wsdl_cache_enabled", 0);

        $this->client = new \SoapClient($url, [ 'trace' => 1, 'exception' => 0 ]);
    }

    public function quickMessage($from, $to, $message) {
        $params = [
            'LoginId' => env('USR_ZONG'),
            'LoginPassword' => env('PWD_ZONG'),
            'NumberCsv' => $to,
            'Mask' => $from,
            'Message' => $message,
            'UniCode' => '0',
            'ShortCodePrefered' => 'n',
            'CampaignName' => uniqid(),
            'CampaignDate' => date('n/j/Y h:i:s a', time() + 90),
        ];
        $message = $this->client->BulkSmsv3([
            'objBulkSms' => $params
        ]);
        return $message;
    }

    public function sendCampaign($from, $to, $message) {
        if (count($to) > 1000) {
            $chunks = $this->getChunks($to);
            foreach ($chunks as $chunk) {
                $chunk = implode(',', $chunk);
                $this->quickMessage($from, $chunk, $message);
            }
        }
        return true;
    }

    private function getChunks($numbers) {
        return array_chunk($numbers, 1000);
    }
}