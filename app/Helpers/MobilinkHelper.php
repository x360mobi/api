<?php

namespace App\Helpers;

class MobilinkHelper {
    private $curl_helper;

    public function __construct() {
        $this->curl_helper = new Curl();
    }

    public function quickMessage($from, $to, $message) {
        $url = env('URL_MOBILINK') . '/sendsms_url.html?Username=' . env('USR_MOBILINK') . '&Password=' .
            env('PWD_MOBILINK') . '&From=' . urlencode($from) . '&To=' . $to . '&Message=' . urlencode($message);

        $response = $this->curl_helper->getPlain($url);
        if ($response == "Message Sent Successfully!") {
            return $response;
        }
        abort(400, $response);
    }

    public function sendCampaign($mask, $numbers, $message) {
        $url = env('URL_MOBILINK') . '/upload_txt.html';
        $headers = ['Content-Type: multipart/form-data'];
        $params = [
            'Username' => env('USR_MOBILINK'),
            'Password' => env('PWD_MOBILINK'),
            'From' => $mask,
            'Message' => $message,
        ];
        $numbers = implode("\r\n", $numbers);

        if (!file_exists(base_path('public') . '/txts'))
            mkdir(base_path('public') . '/txts', 0777);

        $filename = date('YmdHis') . mt_rand(100000, 999999) . '.txt';
        file_put_contents(base_path('public') . '/txts/' . $filename, $numbers);

        $params['file_contents'] = new \CURLFile(base_path('public') . '/txts/' . $filename);

        $this->curl_helper->post($url, $params, $headers);
        unlink(base_path('public') . '/txts/' . $filename);
        return true;
    }
}