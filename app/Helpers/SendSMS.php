<?php

namespace App\Helpers;

class SendSMS {

    public function segregateNumbers($phone_numbers) {
        $mobilink = [];
        $telenor = [];
        $ufone = [];
        $zong = [];
        foreach ($phone_numbers as $phone_number) {
            $matches = [];
            // mobilink
            if ( preg_match('/30[0-9]{1}[0-9]{7}/', $phone_number['number'], $matches) && !in_array($phone_number['number'], $mobilink) )
                array_push($mobilink, '92' . $matches[0]);
            // telenor
            if ( preg_match('/34[0-9]{1}[0-9]{7}/', $phone_number['number'], $matches) && !in_array($phone_number['number'], $telenor) )
                array_push($telenor, '92' . $matches[0]);
            // ufone
            if ( preg_match('/33[0-9]{1}[0-9]{7}/', $phone_number['number'], $matches) && !in_array($phone_number['number'], $ufone) )
                array_push($ufone, '92' . $matches[0]);
            // zong
            if ( preg_match('/31[0-9]{1}[0-9]{7}/', $phone_number['number'], $matches) && !in_array($phone_number['number'], $zong) )
                array_push($zong, '92' . $matches[0]);
            // warid
            if ( preg_match('/32[0-9]{1}[0-9]{7}/', $phone_number['number'], $matches) && !in_array($phone_number['number'], $zong) )
                array_push($zong, '92' . $matches[0]);
        }
        return [ $mobilink, $telenor, $ufone, $zong ];
    }

    public function sendMessages($name, $numbers, $message, $mask, $list_id, $list_name, $telenor_id) {
        list($mobilink, $telenor, $ufone, $zong) = $numbers;

        $this->sendToMobilink($mobilink, $message, $mask);
        $this->sendToTelenor($name, $telenor, $message, $mask, $list_id, $list_name, $telenor_id);
        $this->sendToZong($zong, $message, $mask);
//        $this->sendToUfone($ufone, $message, $mask);
    }

    public function sendQuickMessage($number, $message, $mask) {
        $matches = [];
        // mobilink
        if ( preg_match('/30[0-9]{1}[0-9]{7}/', $number, $matches) )
            return $this->sendToMobilinkQuick('0' . $matches[0], $message, $mask);
        // telenor
        if ( preg_match('/34[0-9]{1}[0-9]{7}/', $number, $matches) )
            $this->sendToTelenorQuick('92' . $matches[0], $message, $mask);
        // ufone
        if ( preg_match('/33[0-9]{1}[0-9]{7}/', $number, $matches) )
            $this->sendToUfoneQuick('92' . $matches[0], $message, $mask);
        // zong
        if ( preg_match('/31[0-9]{1}[0-9]{7}/', $number, $matches) )
            $this->sendToZongQuick('92' . $matches[0], $message, $mask);
        // warid
        if ( preg_match('/32[0-9]{1}[0-9]{7}/', $number, $matches) )
            $this->sendToTelenorQuick('92' . $matches[0], $message, $mask);
    }

    private function sendToMobilink($numbers, $message, $mask) {
        $mobilink = new MobilinkHelper();
        $response = $mobilink->sendCampaign($mask, $numbers, $message);
        return $response;
    }

    private function sendToTelenor($name, $numbers, $message, $mask, $list_id, $list_name, $telenor_id) {
        $telenor = new TelenorHelper();
        $response = $telenor->sendCampaign($name, $numbers, $message, $mask, $list_id, $list_name, $telenor_id);
        return $response;
    }

    private function sendToUfone($numbers, $message, $mask) {}

    private function sendToZong($numbers, $message, $mask) {
        $zong = new ZongHelper();
        $response = $zong->sendCampaign($mask, $numbers, $message);
        return $response;
    }

    private function sendToMobilinkQuick($number, $message, $mask) {
        $mobilink = new MobilinkHelper();
        $response = $mobilink->quickMessage($mask, $number, $message);
        return $response;
    }

    private function sendToTelenorQuick($number, $message, $mask) {
        $telenor = new TelenorHelper();
        $response = $telenor->quickMessage($mask, $number, $message);
        return $response;
    }

    private function sendToUfoneQuick($number, $message, $mask) {
        $ufone = new UfoneHelper();
        $response = $ufone->quickMessage($mask, $number, $message);
        return $response;
    }

    private function sendToZongQuick($number, $message, $mask) {
        $zong = new ZongHelper();
        $response = $zong->quickMessage($mask, $number, $message);
        return $response;
    }
}