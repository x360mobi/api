<?php

namespace App\Helpers;

class Curl {

    public static function post($url , $params = [] , $headers = [] , $auth = [] , $json = false)
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_VERBOSE , true);

        curl_setopt($req, CURLOPT_POST, true );
        
        if($json)
            curl_setopt($req, CURLOPT_POSTFIELDS, json_encode($params));
        else
            curl_setopt($req, CURLOPT_POSTFIELDS, $params);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 2 );
        curl_setopt($req, CURLOPT_SSLVERSION, 6 );
        curl_setopt($req, CURLINFO_HEADER_OUT, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);
        if(!empty($auth))
            curl_setopt($req, CURLOPT_USERPWD, $auth['username'] . ':' . $auth['password']);  
        $resp = curl_exec($req);
        $respCode = curl_getinfo($req);
        curl_close($req);
        return $resp;
    }

    public static function get($url , $params = [] , $headers = [] , $auth = [])
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 2 );
        curl_setopt($req, CURLINFO_HEADER_OUT, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);
        
        $resp = curl_exec($req);
        $respCode = curl_getinfo($req);
        curl_close($req);

//        dd($resp);

        return json_decode($resp , true);
    }

    public static function getPlain($url , $params = [] , $headers = [] , $auth = [])
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 2 );
        curl_setopt($req, CURLINFO_HEADER_OUT, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);

        $resp = curl_exec($req);
        $respCode = curl_getinfo($req);
        curl_close($req);

//        dd($resp);

        return $resp;
    }

    public static function getXML($url , $params = [] , $headers = [] , $auth = [])
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 2 );
        curl_setopt($req, CURLINFO_HEADER_OUT, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);

        $resp = curl_exec($req);
        $respCode = curl_getinfo($req);
        curl_close($req);
        return $resp;
    }

    public static function multiGet($url, $count, $limit, $params=[], $headers=[], $auth=[]) {
        $requests = [];
        $items = [];
        $multi_handle = curl_multi_init();

        $num_requests = ceil($count / $limit);

        for ($i=0; $i<$num_requests; $i++) {
            $start = $i*$limit;
            $new_url = "$url&start=$start&rows=$limit";
            $requests[$i] = curl_init($new_url);

            curl_setopt($requests[$i], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($requests[$i], CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($requests[$i], CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt($requests[$i], CURLOPT_SSL_VERIFYHOST, 2 );
            curl_setopt($requests[$i], CURLINFO_HEADER_OUT, true);
            curl_setopt($requests[$i], CURLOPT_HTTPHEADER, $headers);

            curl_multi_add_handle($multi_handle, $requests[$i]);
        }

        $running = null;
        do {
            curl_multi_exec($multi_handle, $running);
        } while ($running > 0);

        foreach ($requests as $request) {
            $content = json_decode(curl_multi_getcontent($request), true);
            $items = array_merge($items, $content['listing']);
            curl_multi_remove_handle($multi_handle, $request);
        }
        curl_multi_close($multi_handle);

        return $items;
    }

    public static function delete($url , $params = [] , $headers = [] , $auth = [])
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 2 );
        curl_setopt($req, CURLINFO_HEADER_OUT, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);
        
        $resp = curl_exec($req);
        $respCode = curl_getinfo($req);
        curl_close($req);
        return $resp;
    }
}