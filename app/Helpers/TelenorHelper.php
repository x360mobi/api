<?php

namespace App\Helpers;

use App\Models\BaseList;

class TelenorHelper {
    private $curl_helper, $xml_parser, $session_id;

    public function __construct() {
        $this->curl_helper = new Curl();
        $this->session_id = $this->authenticate();
    }

    private function authenticate() {
        $url = env('URL_TELENOR') . 'auth.jsp?msisdn=' . env('USR_TELENOR') . '&password=' .
            env('PWD_TELENOR');
        $response = $this->curl_helper->getXML($url);
        if ($response) {
            $data = $this->parseXML($response);
            if ($data[3]['tag'] == 'RESPONSE' && $data[3]['value'] == 'OK')
                return $data[2]['value'];
        }
        abort(500, 'Not authorized to send messages.');
    }

    public function quickMessage($from, $to, $message) {
        $url = env('URL_TELENOR') . '/sendsms.jsp?session_id=' . $this->session_id . '&to=' . $to . '&text=' . urlencode($message) . '&mask=' . $from;
        $response = $this->curl_helper->getXML($url);
        if ($response) {
            $data = $this->parseXML($response);
            if ($data[2]['value'] == 'Error 103')
                abort(400, 'This mask has not been approved for usage by Telenor yet.');
            else
                return $data[2]['value'];
        }
        abort(500, "Could not send message");
    }

    private function parseXML($response) {
        $data = [];
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $response, $data);
        xml_parser_free($xml_parser);
        return $data;
    }

    public function sendCampaign($name, $from, $to, $message, $list_id, $list_name, $telenor_id) {
        if (!$telenor_id) {
            $telenor_id = $this->createSubscriberList($list_id, $list_name, $to);
        }
        $this->createCampaign($name, $telenor_id, $message, date('Y-m-d H:i:s'), $from);
        return true;
    }

    private function createSubscriberList($list_id, $name, $numbers) {
        $url = env('URL_TELENOR') . '/list.jsp?session_id=' . $this->session_id . '&list_name=' . date('YmdHis') . str_replace(' ', '', $name);

        $response = $this->curl_helper->getXML($url);
        if ($response) {
            $data = $this->parseXML($response);

            if ($data[3]['value'] == 'OK') {
                $id = $data[2]['value'];
                BaseList::where('_id', $list_id)->update([ 'telenor_id' => $id ]);

                $add_url = env('URL_TELENOR') . '/addcontacts.jsp?session_id=' . $this->session_id . '&list_id=' . $id . '&to=' . implode($numbers);

                $add_response = $this->curl_helper->getXML($add_url);
                if ($add_response) {
                    $add_data = $this->parseXML($add_response);
                    if ($add_data[3]['value'] == 'OK')
                        return $id;
                }
            }
        }
        abort(500, "Could not create the list");
    }

    private function createCampaign($name, $list, $message, $time, $mask) {
        $url = env('URL_TELENOR') . '/campaign.jsp?session_id=' . $this->session_id . '&name=' . $name .
            '&group_ids=' . $list . '&text=' . $message . '&time=' . urlencode($time) . '&mask=' . $mask;

        $response = $this->curl_helper->getXML($url);
        if ($response) {
            $data = $this->parseXML($response);
            if ($data[3]['value'] == 'OK')
                return true;
        }
        abort(500, "Could not create telenor campaign");
    }
}