<?php

namespace App\Helpers;

class UfoneHelper {
    private $curl_helper;

    public function __construct() {
        $this->curl_helper = new Curl();
    }

    public function quickMessage($from, $to, $message) {
        if (!substr($to, 0, 2) == '92') {
            if ($to[0] == '0')
                $to = '92'.substr($to, 1);
            elseif ($to[0] == '3')
                $to = '92'.$to;
        }
        $url = env('URL_UFONE') . '?id=' . env('USR_UFONE') . '&password=' .
            env('PWD_UFONE') . '&shortcode=' . urlencode($from) . '&mobilenum=' . $to . '&message=' . urlencode($message) . '&lang=English';

        $response = $this->curl_helper->getXML($url);

        if ($response) {
            return true;
        }

        abort(500, "Could not send message");
    }

    public function sendMessage($numbers, $message, $mask) {
        foreach ($numbers as $number) {
            $this->quickMessage($mask, $number, $message);
        }
        return true;
    }
}