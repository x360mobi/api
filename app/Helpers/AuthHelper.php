<?php

namespace App\Helpers;

use App\Models\User;
use Lcobucci\JWT\Parser;

class AuthHelper {
    public static function authorizeUser() {
        $user = self::getUser();

        if (!$user)
            return null;

        //TODO: ADD ORIGIN CHECK & IF THE USER ROLE IS ALLOWED TO VISIT THIS ORIGIN.
        return $user;
    }

    private static function getUser() {
        $user_id = self::parseToken();
        if ($user_id)
            return User::find($user_id);

        return null;
    }

    private static function parseToken() {
        $token = self::fetchToken();

        if (!$token)
            return null;

        $parser = (new Parser())->parse((string) $token);
        return $parser->getClaim('user');
    }

    private static function fetchToken() {
        $token = app('request')->get('token');

        if (!$token) {
            $headers = app('request')->header('Authorization');
            $parts = explode(' ', $headers);

            if (count($parts) >= 2)
                $token = array_pop($parts);

            if (!$token)
                return null;
        }
        return $token;
    }
}