<?php

namespace App\Models;

class Balance extends Model
{
    protected $collection = 'balances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'company_id', 'amount' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public static function addOrUpdate($balance) {
        $prev_balance = Balance::where('company_id', $balance['company_id'])->first();
        $added_amount = $balance['amount'];
        if ($prev_balance)
            $balance['amount'] += $prev_balance->amount;

        $added = Balance::updateOrCreate([ 'company_id' => $balance['company_id'] ], collect($balance)->toArray());

        if ($added) {
            $history = new BalanceHistory();
            $history->company_id = $balance['company_id'];
            $history->amount = $balance['amount'];
            $history->actor = $balance['user']['_id'];
            $history->action = 'ADD';
            $history->statement = "{$balance['user']['first_name']} {$balance['user']['last_name']} added PKR $added_amount on " . date('h:iA - M d, Y');
            $history->save();
        }

        return $added;
    }
}
