<?php

namespace App\Models;

class QuickMessage extends Model
{
    protected $collection = 'quick_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'number', 'company_id', 'mask_id', 'message' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public static function addOrUpdate($message) {
        return QuickMessage::updateOrCreate([ '_id' => $message['_id'] ], $message);
    }

    public function mask() {
        return $this->hasOne('App\Models\Mask', '_id', 'mask_id');
    }
}
