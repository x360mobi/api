<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Lcobucci\JWT\Builder as JWT;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'first_name', 'last_name', 'email', 'username', 'password', 'phone', 'role', 'last_login', ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [ 'password', ];

    protected $dates = ['last_login'];

    public static function addOrUpdate($user) {
        $user['password'] = Hash::make($user['password']);
        return User::updateOrCreate([ 'email' => $user['email'] ], $user);
    }

    public static function login($credentials) {
        $user = User::where('username', $credentials['username'])->first();

        if ($user && Hash::check($credentials['password'], $user->password)) {
            $signer = new Sha256();

            $user->last_login = date('Y-m-d H:i:s');
            $user->save();

            $token = (new JWT())->setIssuer(env('API_URL'))
                ->setAudience(env('APP_URL'))
                ->setId(str_random(10), true)
                ->setIssuedAt(time())
                ->setNotBefore(time())
                ->setExpiration(time() + 3600)
                ->set('user', $user->_id)
                ->sign($signer, env('APP_KEY'))
                ->getToken();

            $user_data = collect($user)->only('first_name', 'last_name', 'email', 'username', 'phone', 'password', 'last_login');

            $company = Company::where('user_id', $user->_id)->first();
            if ($company)
                $user_data['company'] = $company;

            $user_data['token'] = (string)$token;
            return $user_data;
        }

        return false;
    }
}
