<?php

namespace App\Models;

class BalanceHistory extends Model
{
    protected $collection = 'balance_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'company_id', 'amount', 'actor', 'action', 'statement' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public static function addOrUpdate($history) {
        
        return Balance::updateOrCreate([ '_id' => $history['_id'] ], $history);
    }
}
