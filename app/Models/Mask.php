<?php

namespace App\Models;

class Mask extends Model
{
    protected $collection = 'masks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'company_id' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public static function addOrUpdate($mask) {
        return Mask::updateOrCreate([ '_id' => $mask['_id'] ], $mask);
    }

    public function company() {
        return $this->belongsTo('App\Models\Company', '_id', 'company_id');
    }
}
