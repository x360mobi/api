<?php

namespace App\Models;

class Campaign extends Model
{
    protected $collection = 'campaigns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'date', 'start_time', 'end_time', 'mask_id', 'notes', 'message', 'base_list_id', 'user_id', 'company_id', 'approved', 'sent', 'failed', 'has_run', 'type' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    protected $dates = [ 'date', 'start_time', 'end_time' ];

    public static function addOrUpdate($campaign) {
        return Bundle::updateOrCreate([ '_id' => $campaign['_id'] ], $campaign);
    }

    public function mask() {
        return $this->hasOne('App\Models\Mask', '_id', 'mask_id');
    }

    public function baseList() {
        return $this->hasOne('App\Models\BaseList', '_id', 'base_list_id');
    }

    public function user() {
        return $this->hasOne('App\Models\User', '_id', 'user_id');
    }
}
