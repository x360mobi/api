<?php

namespace App\Models;

class Company extends Model
{
    protected $collection = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'phone', 'package_id', 'rate_per_sms', 'city', 'address', 'ntn_number', 'cnic', 'user_id' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public static function addOrUpdate($company) {
        return Company::updateOrCreate([ '_id' => $company['_id'] ], $company);
    }

    public function user() {
        return $this->hasOne('App\Models\User', '_id', 'user_id');
    }

    public function masks() {
        return $this->hasMany('App\Models\Mask', 'company_id', '_id');
    }

    public function package() {
        return $this->hasOne('App\Models\Bundle', '_id', 'package_id');
    }

    public function balance() {
        return $this->hasOne('App\Models\Balance', 'company_id', '_id');
    }
}
