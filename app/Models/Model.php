<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Model extends Eloquent
{
    use SoftDeletes;

    protected $guarded = ['_id', 'created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public function applySearchQuery($query , $value) {
        return $query;
    }

}
