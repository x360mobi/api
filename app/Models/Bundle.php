<?php

namespace App\Models;

class Bundle extends Model
{
    protected $collection = 'bundles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'price', 'messages' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public static function addOrUpdate($bundle) {
        return Bundle::updateOrCreate([ '_id' => $bundle['_id'] ], $bundle);
    }
}
