<?php

namespace App\Models;

class BaseList extends Model
{
    protected $collection = 'base_lists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'company_id', 'numbers', 'user_id', 'telenor_id' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [  ];

    public function company() {
        return $this->belongsTo('App\Models\Company');
    }

    public function user() {
        return $this->hasOne('App\Models\User', '_id', 'user_id');
    }
}
