<?php

namespace App\Providers;

use Jenssegers\Mongodb\Query\Builder;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Builder::macro('getName', function() {
            return 'mongodb';
        });
    }
}
