<?php

namespace App\Providers;

use App\Helpers\AuthHelper;
use App\Traits\Gates;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->initGates();

        $this->app['auth']->viaRequest('api', function ($request) {
//            if ($request->input('api_token')) {
//                return AuthHelper::authorizeUser();
//            }
            return AuthHelper::authorizeUser();
        });
    }

    /**
     * Generic update and delete permission checks.
     */
    private function initGates() {
        Gate::define('IsAdmin', function ($user, $model) {
            return (integer)$user->role === 0;
        });

        Gate::define('IsSelf', function ($user, $model) {
            return $user->_id === $model->_id;
        });

        Gate::define('IsAuthorized', function ($user, $model) {
            return $user->_id === $model->user_id;
        });

        Gate::define('IsAdminOrSelf', function ($user, $model) {
            return $user->_id === $model->_id || (integer)$user->role === 0;
        });

        Gate::define('IsAdminOrAuthorized', function ($user, $model) {
            return $user->_id === $model->user_id || (integer)$user->role === 0;
        });
    }
}
