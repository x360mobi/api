<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (env('APP_ENV') === 'production') {
            if ($e instanceof NotFoundHttpException) {
                $message = !empty($e->getMessage()) ? $e->getMessage() : 'The requested resource could not be found.';
                return $this->setJSONResponse($message, 400);
            } elseif ($e instanceof MethodNotAllowedHttpException) {
                $message = !empty($e->getMessage()) ? $e->getMessage() : 'The requested method is not allowed.';
                return $this->setJSONResponse($message, 405);
            } elseif ($e instanceof HttpException) {
                $message = !empty($e->getMessage()) ? $e->getMessage() : 'The requested method is not allowed.';
                return $this->setJSONResponse($message, 401);
            } else {
                $message = 'Internal server error.';
                return $this->setJSONResponse($message, 500);
            }
        }

        return parent::render($request, $e);
    }

    private function setJSONResponse($message, $code) {
        $error = [
            'message' => $message,
            'code' => $code
        ];
        return response()->json($error, $code);
    }
}
