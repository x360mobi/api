<?php

namespace App\Http\Controllers;

use App\Traits\Gates;
use Illuminate\Http\Request;

class APIController extends Controller {

    use Gates;

    protected $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function index(Request $request) {
        $query = $this->model;

        $page_size = $request->has('limit') ? intval($request->get('limit')) : intval(env('PAGE_SIZE'));
        $query = $query->take($page_size);
        $query = $query->whereNull('deleted_at');

        foreach ($request->all() as $key => $value){
            switch($key){
                case 'q':
                    $query = $this->model->applySearchQuery($query, $value);
                    break;
                case 'p':
                    $query = $query->skip(($request->input('p') - 1) * $page_size);
                    break;
                case 'w':
                    $fields = explode(',', $value);
                    foreach ($fields as $field)
                        $query = $query->with($field);
                    break;
                case 'limit':
                    $query = $query->limit((int)$request->get('limit'));
                    break;
                case 'order':
                    $query = $query->orderBy('id', $value);
                    break;
                case 'like':
                    break;
                case 'not_null':
                    $query = $query->whereNotNull($value);
                    break;
                case 'not_equal':
                    $pair = explode(',', $value);
                    $query = $query->where($pair[0], '!=', $pair[1]);
                    break;
                default:
                    $value = $this->checkType($value);
                    $query->where($key, '=', $value);
            }
        }

//        dd($query->toSql());
        return $query->get();
    }

    private function checkType($value) {
        if($value === 'false') {
            return false;
        }
        else if($value === 'true') {
            return true;
        }

        return $value;
    }

    private function getModel($id) {
        if (is_object($id)) {
            return $id;
        }
        return $this->model->find($id);
    }

    /*
        GET resource/[id]
    */
    public function show($id){
        $response = $this->model->find($id);

        if (!$response || !$response->id){
            abort(404, "The requested resource could not be found.");
        }
        return $response;
    }

    /*
        PUT resource/[id]
    */
    public function update(Request $request, $id){
        $model = $this->getModel($id);
        $model->fill($request->all());
        $model->save();
        return $model;
    }

    /*
        POST resource/
    */
    public function store(Request $request){
        return $this->model->create($request->all());
    }

    /*
        DELETE resource/[id]
    */
    public function destroy(Request $request, $id){
        $model = $this->getModel($id);

        $response = array('success' => false);
        $response["success"] = $model->delete();
        return $response;
    }
}
