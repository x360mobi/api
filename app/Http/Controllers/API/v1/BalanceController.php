<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\APIController;
use App\Models\Balance;
use Illuminate\Http\Request;

class BalanceController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new Balance();
        parent::__construct($this->model);
    }

    public function store(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        $request['user'] = $request->user();

        return $this->model->addOrUpdate($request);
    }

    public function update(Request $request, $id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdmin' ]);

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::destroy($request, $id);
    }
}
