<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\APIController;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new Company();
        parent::__construct($this->model);
    }

    public function index(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::index($request);
    }

    public function store(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::store($request);
    }

    public function show($id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdminOrSelf' ]);

        $company = $this->model->where('_id', $id)->with('user')->first();

        if (!$company || !$company->id){
            abort(404, "The requested resource could not be found.");
        }
        return $company;
    }

    public function update(Request $request, $id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdminOrSelf' ]);

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::destroy($request, $id);
    }
}
