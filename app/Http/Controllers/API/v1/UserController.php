<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use Illuminate\Support\Facades\Hash;

class UserController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new User();
        parent::__construct($this->model);
    }

    public function index(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        if ($request->has('role'))
            $request['role'] = (int)$request['role'];

        return parent::index($request);
    }

    public function store(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return User::addOrUpdate($request->all());
    }

    public function show($id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdminOrSelf' ]);

        return parent::show($id);
    }

    public function update(Request $request, $id) {
        if ($request->has('role'))
            $this->verifyPermissions($request, $id, $this->model, [ 'IsAdmin' ]);
        else
            $this->verifyPermissions($request, $id, $this->model, [ 'IsAdminOrSelf' ]);

        unset($request['_id']);
        if ($request->has('password'))
            $request['password'] = Hash::make($request->get('password'));
        return User::where('_id', $id)->update($request->all());
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $id, $this->model, [ 'IsAdmin' ]);

        return parent::destroy($request, $id);
    }
}
