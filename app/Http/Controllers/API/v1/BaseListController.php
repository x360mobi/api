<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\APIController;
use App\Models\BaseList;
use App\Models\Company;
use Illuminate\Http\Request;

class BaseListController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new BaseList();
        parent::__construct($this->model);
    }

    public function index(Request $request) {
        $request['company_id'] = Company::where('user_id', $request->user()->_id)->first()->_id;
        $bases = parent::index($request);
        foreach ($bases as $base) {
            $base->numbers = count($base->numbers);
        }
        return $bases;
    }

    public function store(Request $request) {
        if ($request->has('id'))
            return $this->update($request, $request->get('id'));

        $request['user_id'] = $request->user()->_id;
        $file = $_FILES['file'];

        $request['numbers'] = $this->formatNumbers($this->getNumbers($file));

        return $this->model->create($request->all());
    }

    private function getNumbers($file) {
        $numbers = [];
	\Log::info($file['type']);
        if ($file['type'] == 'text/csv')
            $numbers = $this->parseCSV($file);
        elseif ($file['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $file['type'] == "application/vnd.ms-excel")
            $numbers = $this->parseXLSX($file);

        return $numbers;
    }

    private function parseCSV($file) {
        $row = 1;
        $numbers = [];
        // position at which phone numbers are present.
        $phone_index = null;
        if (($handle = fopen($file['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row == 1) {
                    if (!$this->isHeaderRow($data)) {
                        if (!$phone_index)
                            $phone_index = $this->getPhoneIndex($data);
                        if ($phone_index !== -1)
                            array_push($numbers, [ 'number' => $data[$phone_index] ]);
                    }
                }
                else {
                    if ($phone_index == null)
                        $phone_index = $this->getPhoneIndex($data);
                    if ($phone_index !== -1)
                        array_push($numbers, [ 'number' => $data[$phone_index] ]);
                }
                $row++;
            }
            fclose($handle);
        }
        return $numbers;
    }

    private function parseXLSX($file) {
        $numbers = [];

        $contacts = \Excel::load($file['tmp_name'])->toArray();
        $phone_index = null;
        foreach ($contacts as $number) {
            if (!$phone_index)
                $phone_index = $this->getPhoneIndex($number);
            array_push($numbers, [ 'number' => $number[$phone_index] ]);
        }
        return $numbers;
    }

    private function isHeaderRow($data) {
        foreach ($data as $index => $item) {
            if (!preg_match('/^(\+)*(92|0)*3[0-9]{9}$/', $item))
                return true;
        }
        return false;
    }

    private function getPhoneIndex($data) {
        foreach ($data as $index => $item) {
            if (preg_match('/^(\+)*(92|0)*3[0-9]{9}$/', $item))
                return $index;
        }
        return -1;
    }

    private function formatNumbers($numbers) {
        foreach ($numbers as $index => $number) {
            $matches = [];
            preg_match('/3[0-9]{9}$/', $number['number'], $matches);
            if (count($matches))
                $numbers[$index]['number'] = '92' . $matches[0];
            else
                unset($numbers[$index]);
        }
        return $numbers;
    }

    public function update(Request $request, $id) {
//        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdminOrAuthorized' ]);

        $base = BaseList::find($id);

        if (count($_FILES)) {
            $file = $_FILES['file'];
            $numbers = $this->formatNumbers($this->getNumbers($file));

            if ($request['update'])
                $numbers = array_merge($base->numbers, $numbers);

            $request['numbers'] = $numbers;
        }

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
//        $this->verifyPermissions($request, $id, $this->model, [ 'IsAdminOrAuthorized' ]);

        return parent::destroy($request, $id);
    }

    public function getBaseList(Request $request) {
        if ($request->has('id') && $request->has('vendor')) {
            $id = $request->get('id');
            $vendor = $request->get('vendor');
            $list = BaseList::find($id);
            $numbers = [];
            foreach ($list->numbers as $number) {
                $matches = [];
                // mobilink
                if ( preg_match('/30[0-9]{1}[0-9]{7}/', $number['number'], $matches) && $vendor == 'mobilink' && !in_array($number['number'], $numbers) )
                    array_push($numbers, $matches[0]);
                // telenor
                if ( preg_match('/34[0-9]{1}[0-9]{7}/', $number['number'], $matches) && $vendor == 'telenor' && !in_array($number['number'], $numbers) )
                    array_push($numbers, $matches[0]);
                // ufone
                if ( preg_match('/33[0-9]{1}[0-9]{7}/', $number['number'], $matches) && $vendor == 'ufone' && !in_array($number['number'], $numbers) )
                    array_push($numbers, $matches[0]);
                // zong
                if ( preg_match('/31[0-9]{1}[0-9]{7}/', $number['number'], $matches) && $vendor == 'zong' && !in_array($number['number'], $numbers) )
                    array_push($numbers, $matches[0]);
                // warid
                if ( preg_match('/32[0-9]{1}[0-9]{7}/', $number['number'], $matches) && $vendor == 'warid' && !in_array($number['number'], $numbers) )
                    array_push($numbers, $matches[0]);
                // all
                if ( $vendor == 'all' && !in_array($number['number'], $numbers) )
                    array_push($numbers, $number['number']);
            }
            $filename = date('YmdHis') . '_' . $list->name . '.csv';
            $full_filename = base_path('public') . '/logs/' . $filename;

            if (!file_exists(base_path('public') . '/logs'))
                mkdir(base_path('public') . '/logs', 0777);

            if (!file_exists($full_filename)) {
                touch($full_filename);

                $fp = fopen($full_filename, 'w');
                foreach ($numbers as $number) {
                    fputcsv($fp, [$number]);
                }
                fclose($fp);
                chmod($full_filename, 0777);

                $response = response()
                    ->download($full_filename, $filename, [
                        'Content-Type' => mime_content_type($full_filename),
                        'Access-Control-Allow-Origin' => '*',
                    ]);
                register_shutdown_function('unlink', $full_filename);
                return $response;
            }
        } else {
            abort(400, "Incomplete arguments.");
        }
    }
}
