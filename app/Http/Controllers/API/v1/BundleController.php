<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\APIController;
use App\Models\Bundle;
use Illuminate\Http\Request;

class BundleController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new Bundle();
        parent::__construct($this->model);
    }

    public function store(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::store($request);
    }

    public function update(Request $request, $id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdmin' ]);

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::destroy($request, $id);
    }
}
