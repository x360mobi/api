<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;

class AuthController extends Controller {
    public function authenticate(Request $request) {
        $credentials = $request->only('username', 'password');

        $user = User::login($credentials);

        if ($user)
            return $user;

        abort(401, 'Invalid credentials.');
    }
}
