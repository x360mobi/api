<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\APIController;
use App\Models\Company;
use App\Models\Mask;
use Illuminate\Http\Request;

class MaskController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new Mask();
        parent::__construct($this->model);
    }

    public function index(Request $request) {
        return parent::index($request);
    }

    public function store(Request $request) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        $company = Company::find( $request->get('company_id') );

        return $company->masks()->create([
            'name' => $request->get('name'),
        ]);
    }

    public function show($id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdmin' ]);

        $company = $this->model->where('_id', $id)->with('company')->first();

        if (!$company || !$company->id){
            abort(404, "The requested resource could not be found.");
        }
        return $company;
    }

    public function update(Request $request, $id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdmin' ]);

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::destroy($request, $id);
    }
}
