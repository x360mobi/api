<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\APIController;
use App\Models\BalanceHistory;
use Illuminate\Http\Request;

class BalanceHistoryController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

        $this->model = new BalanceHistory();
        parent::__construct($this->model);
    }

    public function update(Request $request, $id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdmin' ]);

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdmin' ]);

        return parent::destroy($request, $id);
    }
}
