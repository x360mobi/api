<?php

namespace App\Http\Controllers\API\v1;

use App\Helpers\SendSMS;
use App\Http\Controllers\APIController;
use App\Models\Balance;
use App\Models\BalanceHistory;
use App\Models\Campaign;
use App\Models\Company;
use App\Models\QuickMessage;
use Illuminate\Http\Request;

class CampaignController extends APIController
{
    private $sms_helper;

    public function __construct() {
        $this->middleware('auth');

        $this->model = new Campaign();
        $this->sms_helper = new SendSMS();
        parent::__construct($this->model);
    }

    public function index(Request $request) {
        if ($request->user()->role != 0) {
            $company = Company::where('user_id', $request->user()->_id)->first();
            if ($company)
                $request['company_id'] = $company->_id;
        }
        return parent::index($request);
    }

    public function store(Request $request) {
        $request['user_id'] = $request->user()->_id;
        $request['approved'] = false;
        $request['has_run'] = false;

        return parent::store($request);
    }

    public function update(Request $request, $id) {
        $this->verifyPermissions(app('request'), $id, $this->model, [ 'IsAdminOrAuthorized' ]);

        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        $this->verifyPermissions($request, $request->user()->_id, $this->model, [ 'IsAdminOrAuthorized' ]);

        return parent::destroy($request, $id);
    }

    public function sendQuickMessage(Request $request) {
        if ( $request->has([ 'mask', 'number', 'message' ]) ) {
            if ($request->has('log_history') && $request->get('log_history')) {
                $company_id = $request->get('company_id');

                QuickMessage::create([
                    'number' => $request->get('number'),
                    'company_id' => $company_id,
                    'mask_id' => $request->get('mask_id'),
                    'message' => $request->get('message'),
                ]);

                $company = Company::where('_id', $company_id)->with('package')->with('balance')->first();

                if (!$company->package)
                    abort(400, 'No company selected.');
                if (!$company->balance)
                    abort(400, 'No balance added.');
                if ($company->balance->amount < $company->package->price)
                    abort(400, 'Not enough balance.');

                Balance::where('company_id', $company_id)->update([ 'amount' => $company->balance->amount - $company->package->price ]);
                BalanceHistory::create([
                    'company_id' => $company_id,
                    'amount' => $company->package->price,
                    'actor' => $request->user()->_id,
                    'action' => 'SUBTRACT',
                    'statement' => $request->user()->first_name . ' ' . $request->user()->last_name . ' sent a quick message to ' . $request->get('number') . '.',
                ]);
            }
            return $this->sms_helper->sendQuickMessage($request->get('number'), $request->get('message'), $request->get('mask'));
        }
        abort(400, 'Incomplete parameters.');
    }

    public function getQuickMessages(Request $request) {
        if ($request->has('company_id')) {
            if ($request->has('limit'))
                return QuickMessage::where('company_id', $request->get('company_id'))->with('mask')->limit((int)$request->get('limit'))->get();
            return QuickMessage::where('company_id', $request->get('company_id'))->with('mask')->get();
        }
        abort(400, 'No company selected.');
    }

    public function getLogs(Request $request) {
        if ($request->has('campaign_id')) {
            $id = $request->get('campaign_id');
            $campaign = Campaign::where('_id', $id)->with('mask')->with('baseList')->first();
            if ($campaign && $campaign->has_run) {
                $logs = array();
                $numbers = $campaign->baseList->numbers;
                $failed_count = (int)(count($campaign->baseList->numbers) * env('FAILED_PERCENTAGE') / 100);

                if ($failed_count > 0) {
                    $sample = array_rand($numbers, $failed_count);
                    $items = array();
                    foreach ($sample as $index) {
                        $logs[] = [$numbers[$index]->number, 'Failed'];
                        $items[] = $numbers[$index];
                    }
                    $numbers = array_diff($numbers, $items);
                }
                foreach ($numbers as $index => $number) {
                    $logs[] = [$number['number'], 'Sent'];
                }

                $filename = date('YmdHis') . '_logs_' . str_replace(' ', '', $campaign->name) . '.csv';
                $full_filename = base_path('public') . '/logs/' . $filename;

                if (!file_exists(base_path('public') . '/logs'))
                    mkdir(base_path('public') . '/logs', 0777);

                if (!file_exists($full_filename)) {
                    touch($full_filename);

                    $fp = fopen($full_filename, 'w');
                    foreach ($logs as $log) {
                        fputcsv($fp, $log);
                    }
                    fclose($fp);
                    chmod($full_filename, 0777);

                    $response = response()
                        ->download($full_filename, $filename, [
                            'Content-Type' => mime_content_type($full_filename),
                            'Access-Control-Allow-Origin' => '*',
                        ]);
                    register_shutdown_function('unlink', $full_filename);
                    return $response;
                }
            }
            abort(400, 'No campaign found.');
        }
        abort(400, 'No campaign selected.');
    }

    public function getQuickLogs(Request $request) {
        if ($request->has('company_id')) {
            $id = $request->get('company_id');
            $to = $request->get('to');

            if ($request->has('from') && !empty($request->get('from'))) {
                $from = $request->get('from');
                $messages = QuickMessage::where('company_id', $id)->where('from', '>=', $from.' 00:00:00')->where('to', '<=', $to.' 23:59:59')->with('mask')->get();
            } else {
                $messages = QuickMessage::where('company_id', $id)->where('to', '<=', $to.' 23:59:59')->with('mask')->get();
            }

            $filename = date('YmdHis') . '_logs_quick_messages' . '.csv';
            $full_filename = base_path('public') . '/logs/' . $filename;

            if (!file_exists(base_path('public') . '/logs'))
                mkdir(base_path('public') . '/logs', 0777);

            if (!file_exists($full_filename)) {
                touch($full_filename);

                $fp = fopen($full_filename, 'w');
                foreach ($messages as $message) {
                    $message_flat = [
                        $message['created_at'],
                        $message['number'],
                        $message['mask']['name'],
                        $message['message']
                    ];
                    fputcsv($fp, $message_flat);
                }
                fclose($fp);
                chmod($full_filename, 0777);

                $response = response()
                    ->download($full_filename, $filename, [
                        'Content-Type' => mime_content_type($full_filename),
                        'Access-Control-Allow-Origin' => '*',
                    ]);
                register_shutdown_function('unlink', $full_filename);
                return $response;
            }
            abort(400, 'No campaign found.');
        }
        abort(400, 'No campaign selected.');
    }
}
