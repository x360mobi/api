<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends APIController
{
    public function __construct() {
        $this->middleware('auth');

//        UNCOMMENT THIS LINE AND REPLACE WITH APPROPRIATE MODEL.
//        $this->model = new User();
        parent::__construct($this->model);
    }

    public function index(Request $request) {
        return parent::index($request);
    }

    public function store(Request $request) {
        return parent::store($request);
    }

    public function show($id) {
        return parent::show($id);
    }

    public function update(Request $request, $id) {
        return parent::update($request, $id);
    }

    public function destroy(Request $request, $id) {
        return parent::destroy($request, $id);
    }
}
