<?php namespace App\Console\Commands;

use App\Helpers\SendSMS;
use App\Models\Balance;
use App\Models\BalanceHistory;
use App\Models\Campaign;
use App\Models\Company;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class CheckCampaignsCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'check-campaigns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Run the campaigns";

    /**
     * The signature of console command.
     *
     * @var string
     */
    protected $signature = 'check-campaigns';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        // get all the APPROVED campaigns NOT RUN YET.
        $campaigns = Campaign::where('approved', true)->where('has_run', false)->where('start_time', '<', new \DateTime())->with('baseList')->with('mask')->get();
        $send_sms = new SendSMS();

        foreach ($campaigns as $campaign) {
            $company = Company::where('_id', $campaign->company_id)->with('package')->with('balance')->first();
            $total_amount = $company->rate_per_sms * count($campaign->baseList->numbers);

            if (
                $company->package &&
                $company->balance &&
                $company->balance->amount >= $total_amount
            ) {
                Balance::where('company_id', $campaign->company_id)->update([ 'amount' => $company->balance->amount - $total_amount ]);
                BalanceHistory::create([
                    'company_id' => $campaign->company_id,
                    'amount' => $total_amount,
                    'action' => 'SUBTRACT',
                    'statement' => $campaign->name . ' was run.',
                ]);

                $segregated_numbers = $send_sms->segregateNumbers($campaign->baseList->numbers);
                $send_sms->sendMessages(
                    $campaign->name,
                    $segregated_numbers,
                    $campaign->message,
                    $campaign->mask->name,
                    $campaign->baseList->_id,
                    $campaign->baseList->name,
                    isset($campaign->baseList->telenor_id) ? $campaign->baseList->telenor_id : null
                );

                $campaign->has_run = true;
                $campaign->save();
            }
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
        );
    }

}