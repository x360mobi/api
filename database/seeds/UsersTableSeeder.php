<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder {
    public function run() {
        DB::table('users')->insert([
            'first_name' => 'Ahmed',
            'last_name' => 'Qazi',
            'email' => 'ahmed.h.qazi@gmail.com',
            'password' => Hash::make('password'),
            'role' => 0
        ]);
    }
}